resource "google_compute_firewall" "http" {
  name    = "http-for-nginx"
  network = "default"
  allow {
    protocol = "tcp"
    ports    = ["80"]
  }
  source_ranges = ["0.0.0.0/0"]
}
